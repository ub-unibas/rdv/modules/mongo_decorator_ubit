# mongo_decorator_ubit

mongo decorator for function calls to speed up and store calls with static response, requires mongo for storage

## functionalities

### usage
* use as function decorator: 
```
@mongo_decorator
def get_test(key=Test):
    return key
 ```
* use from within code
```
mongo_store.set_keyvalue_cache(key="Test", value=2)
mongo_store.get_keyvalue_cache(key="Test")
```

#### hints
* instantiate MongoDecorator before using it, if used multiple times (speed up)
* use param-variables to create useful keys when used as decorator, even if params are not used within function

### return values
the MongoDecorator tries to return an internal data structure,
so if a json blob is stored it will be returned using json.loads()

### store empty
if a function returns a value that is false, only a mongo entry is created,
if store_empty is definied within the configuration (see mongo_emptyvalues.yaml [standard cfg])

### key-generation
the lookup key is built from the function name and the params that are passed.
If the MongoDecorator is used on a class method, within this class a set_cache_key and get_cache_key method
can be defined which overwrites this standard behavior (function name + params)

### logging
if the Mongo Decorator is used for a class method
and the class has a logger property this is used for logging,
otherwise cfg/logger_mongo.yaml from module is used as a fallback logging configuration


## config-files
create MongoDecorator classes using cfg-files and user credentials:

```MongoDecorator = create_mongo_decorator(config_file= "/tmp/mongo.yaml", user=os.getenv("MONGO_USER"), password=os.getenv("MONGO_PWD"))```

instantiate MongoDecorator with db and table name 

```mongo_store = MongoDecorator(db_name="slsp_oai_harvesting", collection_name="zas")```

ENV-Variable MODE defines which setting from config file is used: default is debug, to change this:

```export MODE=test```

## known issues
sometimes dictionaries throw errors when being pickled ("maximum recursion depth exceeded")

better logic which params are passed when creating MongoDecorator class 
and when instantiating mongostore object

## example
```
from mongo_decorator_ubit.mongo_decorator import create_mongo_decorator

from sickle import Sickle
from xml.etree import ElementTree

MongoDecorator = create_mongo_decorator(config_file=/tmp/mongo_emptyvalues.yaml")

mongo_store = MongoDecorator(db_name="test_dec", collection_name="testing")

mongo_store.set_keyvalue_cache(key="Test2", value=2)
print(mongo_store.get_keyvalue_cache(key="Test2"))
mongo_store.set_keyvalue_cache(key="Test2", value=3)
print(mongo_store.get_keyvalue_cache(key="Test2"))

@mongo_store
def test_mongo(a):
    return {"test": list([a, "b"])}

print(test_mongo(a={1,2,3}))
```