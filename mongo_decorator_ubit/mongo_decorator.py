import os
import json
import yaml
import time
import pickle
import inspect
import logging.config

from datetime import datetime, timezone, timedelta
import functools
from typing import Any, Callable
from pkg_resources import Requirement, resource_filename

from pytz import reference

from pymongo import MongoClient
from bson.objectid import ObjectId
import pymongo

MODE = os.getenv('MODE') or "debug"

def create_mongo_decorator(config_file = None, config_dict= {}, db_name=None, collection_name=None, user=None, password=None):
    if config_file:
        config_data = yaml.load(open(config_file), Loader=yaml.SafeLoader)
    elif config_dict:
        config_data = config_dict

    if MODE in ["prod"]:
        config = config_data.get("prod",{})
    elif MODE in ["container"]:
        config = config_data.get("container",{})
    elif MODE in ["k8s"]:
        config = config_data.get("k8s",{})
    elif MODE in ["test"]:
        config = config_data.get("test",{})
    else:
        config = config_data.get("debug",{})

    if not config:
        config = config_data

    class MongoLoaderConfig(MongoLoader):

        """
        :param db: mongo db to be used for storing

        """

        _host = config.get("host")
        _port = config.get("port")
        _db_name = db_name
        _user = user or config.get("user")
        _pwd = password or config.get("pwd")
        _store_empty = config.get("store_empty", [None, False, 0, ""])

    return MongoLoaderConfig


class MongoLoader:
     # dict to store initialized redis databases
    initialized = {}

    def build_mongo_con_url(self):
        """because with params it could not connect ..."""
        from urllib.parse import quote_plus
        conn_url = ""
        if self._port:
            conn_url = "{}:{}".format(self._host, self._port)
        conn_url=conn_url.replace("mongodb://", "")
        if self._user and self._pwd:
            conn_url = "{}:{}@{}".format(quote_plus(self._user), quote_plus(self._pwd),conn_url)
        conn_url = "mongodb://{}".format(conn_url)
        return conn_url

    def __init__(self, db_name=None, collection_name=None):
        """ flexible Cache Class based on redis for various purposes
        (testing, iiif-cache, api-call cache, gspreadsheet cache)
        :param last_change_date_str: datetime in date.now.isoformat
            to check if cache was created after last_change_date_str """
        if db_name:
            self._db_name = db_name
        if collection_name:
            self._collection_name = collection_name
        mongo_con_url = self.build_mongo_con_url()
        client = MongoClient(mongo_con_url)

        db = client.get_database(self._db_name)
        if not self._collection_name in db.list_collection_names():
            collection = db.get_collection(self._collection_name)
            collection.create_index([('_store_key', pymongo.ASCENDING)], unique=True)
        self.db = db

    def store_version(self, obj_data={}, old_obj_data= {}, new_obj_id=None):
        # for new objects id is passed
        obj_id = new_obj_id or self.obj_id
        collection = self.db.get_collection(self.obj_type)
        versions = self.get_versions(obj_id=obj_id, col_name=self.obj_type)
        # falls noch keine Version speichere Original-Version
        if not versions and old_obj_data:
            old_obj = self.filter_fields(old_obj_data)
            old_obj.update({"_obj_id": obj_id})
            collection.insert_one(old_obj)
            # sleep 1 second so orig version timestamp is always first
            time.sleep(1)
        filtered_obj = self.filter_fields(obj_data)
        filtered_obj.update({"_store_key": obj_id})
        result = collection.insert_one(filtered_obj)
        return result.inserted_id

    @classmethod
    def load_version(cls, store_id, col_name):
        collection = cls.db.get_collection(col_name)
        last_obj = collection.find_one({'_id': ObjectId(store_id)})
        del last_obj["_id"]
        del last_obj["_obj_id"]
        return last_obj

    def __call__(self, fn: Callable) -> Callable:
        """ create decorator for functions to use caching mechanism

        :param fn: function which returns value shall be cached or loaded from Cache
        :return: cache_func
        """

        @functools.wraps(fn)
        def cache_func(*args, **kwargs) -> Any:
            """ returns value from orig function either from cache or via orig function calls

            :param args: positional args from orig function
            :param kwargs: keyword args from orig function
            :return: value from cache or from fn call
            """

            if args:
                class_ = args[0] if inspect.isclass(args[0]) else args[0].__class__
                if hasattr(args[0], "logger"):
                    self.logger = args[0].logger
                else:
                    self.logger = self.get_logger()
            else:
                class_ = None
                self.logger = self.get_logger()

            # options how to build Cache-Path, difference between setter and getter,
            # so cached results can be reused for further refinement (e.g. autocomplete)
            # each class that uses a decorator can define it's own set and get function for the cache keys
            if class_ and hasattr(class_, "set_cache_key"):
                cache_getkey = class_.get_cache_key(*args, **kwargs, fn=fn)
                cache_setkey = class_.set_cache_key(*args, **kwargs, fn=fn)
            elif class_ and hasattr(class_, "get_cache_key"):
                cache_getkey = class_.get_cache_key(*args, **kwargs, fn=fn)
                cache_setkey = cache_getkey
            # otherwise a standard function to generate cache keys is used
            else:
                cache_getkey = self.get_cache_key(*args, **kwargs, fn=fn)
                cache_setkey = cache_getkey
                self.logger.debug("No Mongo key function defined. Cache key " + cache_getkey + " built.")

            self.cache_getkey = cache_getkey
            self.cache_setkey = cache_setkey

            use_cache = self._cache_exists()
            localtime = reference.LocalTimezone()

            # return cache and log reason for caching
            if use_cache:
                message_temp = "Mongo used: {0}, last changed"
                message = message_temp.format(self.cache_getkey)
                self.logger.info(message)
                stored_data = self._get_cache()
                if stored_data:
                    try:
                        try:
                            return json.loads(stored_data)
                        except (json.JSONDecodeError, UnicodeDecodeError):
                            unpickle = pickle.loads(stored_data)
                            try:
                                return json.loads(unpickle)
                            except Exception:
                                return unpickle
                    except Exception as e:
                        self.logger.critical("Mongo Error: " + self.cache_getkey + " can not be opened: " + str(e))
                else:
                    return self._get_cache()
            else:
                # if no cache call function with arguments and set cache
                return_value = fn(*args, **kwargs)
                if return_value or return_value in self._store_empty:
                    self._set_cache(return_value)
                    self.logger.info("Mongo-Entry created for " + self.cache_setkey)
                else:
                    self.logger.debug("No Mongo-Entry created for " + self.cache_setkey + " because empty return value")
                return return_value

        return cache_func

    def get_cache_key(self, *args, fn: Callable ="", unique_id ="", **kwargs) -> str:
        """ build cache key from function arguments and function name
            shall be overwritten to return better cache-keys

        :param self: self can be used to access the object specific arguments from a function
        :param args: positional args from funct
        :param fn: function for which cache decorator shall be used
        :param kwargs: keyword args from funct
        :return: cache key built from object/functions arguments
        """
        if unique_id:
            cache_key = unique_id
        elif fn:
            fn = fn
            func_name = fn.__name__
            arg_names = inspect.getfullargspec(fn).args
            arg_dict = {}
            # get names for args using inspect function and store them in a dict
            for key, value in zip(arg_names, args):
                arg_dict[key] = value
            kwargs.update(arg_dict)
            try:
                del kwargs["self"]
            except KeyError:
                pass

            var_key = "_".join([key + "=" + str(self.sort_json_dump(kwargs[key])) for key in sorted(kwargs.keys())])
            cache_key = func_name + "_" + var_key

        return cache_key

    def _cache_exists(self) -> bool:
        """check if cache for a key exists

        :return: True/False if cache for key exists
        """
        key = self.cache_getkey
        try:
            self.db[self._collection_name].find({"_store_key": key}).next()
            return True
        except StopIteration:
            return False

    def _get_cache(self) -> str:
        """ get value for key from cache or disk

        :return: string that can be used for json.loads()
        """
        key = self.cache_getkey
        try:
            stored_data_blob = self.db[self._collection_name].find({"_store_key": key}).next()
            stored_data = stored_data_blob.get("store_blob")
            return stored_data
        except StopIteration:
            self.logger.warning("Cache could not be loaded: " + key)
            return None

    def _set_cache(self, value: Any) -> None:
        """ set redis cache for any json serializable value (optional with expiration time),
            store also datetime in redis when object was stored
            if data_dump (big dataset e.g. from ES write it also to disk so it will not be removed by Lru Cache)

        :param value: datatype to be transformed to json
        """
        key = self.cache_setkey

        # check if already pickle serialized value
        try:
            json.loads(value)
            value = pickle.dumps(value)
        except (pickle.UnpicklingError, TypeError, json.JSONDecodeError):
            try:
                value = json.dumps(value)
            except TypeError:
                value = pickle.dumps(value)

        store_data = {"_store_key": key, "store_blob": value}
        return self.db[self._collection_name].replace_one({ '_store_key': key }, store_data, upsert=True)

    def get_keyvalue_cache(self, key: Any) -> str:
        """ get value for key from cache or disk

        :return: string that can be used for json.loads()
        """
        try:
            stored_data_blob = self.db[self._collection_name].find({"_store_key": key}).next()
            stored_data = stored_data_blob.get("store_blob")
            try:
                json.loads(stored_data)
                return json.loads(stored_data)
            except (json.JSONDecodeError, UnicodeDecodeError):
                unpickle = pickle.loads(stored_data)
                try:
                    return json.loads(unpickle)
                except Exception:
                    return unpickle
        except StopIteration as e:
            try:
                self.logger.warning("Cache could not be loaded: " + key)
            except AttributeError:
                self.logger = self.get_logger()
                self.logger.warning("Cache could not be loaded: " + key)
            return ""

    def set_keyvalue_cache(self, value: Any, key: Any) -> None:
        """ set redis cache for any json serializable value (optional with expiration time),
            store also datetime in redis when object was stored
            if data_dump (big dataset e.g. from ES write it also to disk so it will not be removed by Lru Cache)

        :param value: datatype to be transformed to json
        """

        # check if already pickle serialized value
        try:
            json.loads(value)
            value = pickle.dumps(value)
        except (pickle.UnpicklingError, TypeError, json.JSONDecodeError):
            try:
                value = json.dumps(value)
            except TypeError:
                value = pickle.dumps(value)
        store_data = {"_store_key": key, "store_blob": value}
        return self.db[self._collection_name].replace_one({ '_store_key': key }, store_data, upsert=True)

    def del_keyvalue_cache(self, key: Any) -> None:
        return self.db[self._collection_name].delete_one({ '_store_key': key })

    @classmethod
    def sort_json_dump(cls, value: Any) -> str:
        """ order nested dictionaries

        :param value: data structure to be sorted
        :return: string serialized json
        """

        if isinstance(value, list):
            a_dump = []
            for a in value:
                # to exclude None or "" Values
                if a == 0 or a:
                    a_copy = cls.sort_json_dump(a)
                    a_dump.append(a_copy)
            a_dump.sort()
        elif isinstance(value, dict):
            a_dump = {}
            for key, value in sorted(value.items()):
                if cls.is_simple_field(value):
                    a_dump[key] = str(value)
                else:
                    a_dump[key] = cls.sort_json_dump(value)
        else:
            if value != 0 and not value:
                # to exclude None values
                value = ""
            if not isinstance(value, str):
                value = str(value)
            return value
        # otherwise empty a_dumps end up as [], {}-String
        if a_dump:
            return json.dumps(a_dump, sort_keys=True, ensure_ascii=False)

    @staticmethod
    def is_simple_field(value):
        """
        :param value:
        :return:
        """
        return isinstance(value, int) or isinstance(value, float) or isinstance(value, str) \
               or isinstance(value, type(None))

    @staticmethod
    def get_logger():
        config = yaml.load(open(resource_filename(Requirement.parse("mongo_decorator_ubit"), "mongo_decorator_ubit/cfg/logger_mongo.yaml")), Loader=yaml.Loader)
        logging.config.dictConfig(config)
        if MODE in ["prod", "test"]:
            return logging.getLogger('cache_prod')
        else:
            return logging.getLogger('cache_debug')
