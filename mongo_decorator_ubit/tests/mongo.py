from mongo_decorator_ubit.mongo_decorator import create_mongo_decorator
from pkg_resources import Requirement, resource_filename

from sickle import Sickle
from xml.etree import ElementTree

MongoDecorator = create_mongo_decorator(
    config_file=resource_filename(Requirement.parse("mongo_decorator_ubit"), "mongo_decorator_ubit/cfg/mongo_emptyvalues.yaml"))

mongo_store = MongoDecorator(db_name="test_dec", collection_name="testing")

mongo_store.set_keyvalue_cache(key="Test2", value=2)
print(mongo_store.get_keyvalue_cache(key="Test2"))
mongo_store.set_keyvalue_cache(key="Test2", value=3)
print(mongo_store.get_keyvalue_cache(key="Test2"))

@mongo_store
def test_mongo(a):
    return {"test": list([a, "b"])}

print(test_mongo(a={1,2,3}))

@mongo_store
def test_oai_mongo(emanu_id):
    sickle = Sickle("https://www.e-manuscripta.ch/oai")
    record = sickle.GetRecord(identifier=emanu_id, metadataPrefix="mets")
    record_str = str(record).encode("utf-8")
    root = ElementTree.fromstring(record_str)
    return root

print(ElementTree.tostring(test_oai_mongo(emanu_id="oai:www.e-manuscripta.ch:3282330")))