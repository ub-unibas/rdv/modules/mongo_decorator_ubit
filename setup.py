import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mongo_decorator_ubit",
    setup_requires=['setuptools-git-versioning'],
    setuptools_git_versioning={
        "enabled": True,
    },
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="cache decorator (using mongo) to store values",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ub-unibas/rdv/modules/mongo_decorator_ubit",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    install_requires=[ 'pyyaml', 'pymongo'],
    include_package_data=True,
    zip_safe=False
)